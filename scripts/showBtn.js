function showBtn(){
    const callBackButton = document.getElementById('btn_callback')

    window.addEventListener("scroll", ()=>{
        if(window.scrollY > 450 && window.scrollY < 7600){
            callBackButton.classList.add('btn_callback--active')
        }else{
            callBackButton.classList.remove('btn_callback--active')
        }

    })

}
showBtn()
function Header(){
    const headerNav = document.querySelector('.header__header')

    window.addEventListener("scroll", ()=>{
        if(window.scrollY > 850){
            headerNav.style.backgroundColor='white';
        }else{
            headerNav.style.backgroundColor='rgba(247, 247, 247, 0.9)';
        }
    })
}
Header()