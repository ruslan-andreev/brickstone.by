function validation (){
    const errorLabel = document.querySelector('.error-label__content');
    const formButton = document.querySelector('.button-form');
    const inputPhone = document.getElementById('input-tel');
    const form = document.getElementById('form')
    let phoneNumber = '';

    
    inputPhone.addEventListener('change', (event)=>{
        phoneNumber = event.target.value;
        validation(phoneNumber)
    })

    formButton.addEventListener('click',(event)=> {
        event.preventDefault()
        formSubmit()
    })

    function checkNumber(phoneNumber){
                
        const regExp = /\+375\s?\(?[0-9]{2}\)?\s?-?\d{3}\s?-?\d{2}\s?-?\d{2}/;
        return regExp.test(phoneNumber)
    }

    function validation(phoneNumber){
        if(checkNumber(phoneNumber)){
            errorLabel.classList.remove('error-label__content--active')
            formButton.disabled = false
        }else{
            errorLabel.classList.add('error-label__content--active')
            formButton.disabled = true
        }
    }
    
    /*отправка формы*/
    async function formSubmit(){
        const data = serializeForm(form);
        const response = await sendData(data);

        if (response.ok){
            let result = await response.json();
            alert("Форма успешно отправлена")
            formReset();
        } else {
           alert("Error:" +  response.status);
        }
    }

    function serializeForm(form){
        return new FormData(form)
    }

    async function sendData(data){
        return await fetch("send_mail.php", {
            method: "POST",
            body: data,
        })
    }
    function formReset(){
        form.reset();
    }
}
validation()