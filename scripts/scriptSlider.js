new Swiper('.swiper',{
    pagination:{
        el: '.swiper-pagination',
        clikable: true
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    
    loop: true,
    autoplay:{
        delay: 5000,
        stopOnLastSlide: false,
        disableOnInteraction: false
    },
    slidesParView: 1,
})

